import svg4everybody from 'svg4everybody';
import $ from 'jquery';

import checkState from '../blocks/quest-checkbox-item/quest-checkbox-item';
import radioState from '../blocks/quest-radio-item/quest-radio-item';
import rangeSlider from '../blocks/quest-range-level/quest-range-level';

$(() => {
	svg4everybody();
	checkState();
	radioState();
	rangeSlider();
});
