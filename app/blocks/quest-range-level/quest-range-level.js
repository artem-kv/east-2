const rangeSlider = () => {
    const wrapper = document.getElementsByClassName('js-range-wrapper');

    [...wrapper].forEach((slider, index) => {
        const range = slider.getElementsByClassName('js-range-input')[0];
        const label = slider.getElementsByClassName('js-range-label');

        [...label].forEach((item, i) => {
            item.onclick = () => {
                if (i === 0) {range.value = 0;}
                if (i === 1) {range.value = 19.2;}
                if (i === 2) {range.value = 48.65;}
                if (i === 3) {range.value = 100;}

                localStorage.setItem(`arrowPos-${index}`, range.value);
            };
        });

        range.onchange = () => {
            localStorage.setItem(`arrowPos-${index}`, range.value);
        };

        if (localStorage.getItem(`arrowPos-${index}`) === null) {range.value = 48.65;}
        else {range.value = localStorage.getItem(`arrowPos-${index}`);}
    });
};
export default rangeSlider;
