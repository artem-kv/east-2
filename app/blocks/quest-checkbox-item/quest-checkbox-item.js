const checkState = () => {
    const checkbox = document.getElementsByClassName('js-check-state');

    [...checkbox].forEach((item, index) => {
        const input = item.children[0];
        const localVal = localStorage.getItem(`check-${index}`);

        item.onchange = () => {
            if (input.hasAttribute('checked')) {input.removeAttribute('checked');}
            else {input.setAttribute('checked', 'checked');}

            localStorage.setItem(`check-${index}`, input.hasAttribute('checked'));
        };

        if (localVal === 'true') {input.setAttribute('checked', 'checked');}
        if (localVal === 'false') {input.removeAttribute('checked');}
    });
};

export default checkState;
