const radioState = () => {
    const radio = document.getElementsByClassName('js-radio-state');
    const localVal = localStorage.getItem('radio');

    [...radio].forEach((item, index) => {
        const input = item.children[0];

        item.onchange = () => {
            localStorage.removeItem('radio');
            localStorage.setItem('radio', index);

            input.removeAttribute('checked');
        };
    });

    if (localVal !== null) {
        radio[localVal].getElementsByTagName('input')[0].setAttribute('checked', 'checked');
    }
};

export default radioState;
